//
//  AssetsModel.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

struct AssetsModel: Identifiable {
    let id: Int
    let image: String
    let name: String
    let value: String
    let desc: String
    let percentValue: String
    let percen: String
    let trade: String
}

var homeAssets: [AssetsModel] = [
    .init(id: 0,
          image: "farm",
          name: "Farmsent",
          value: "747 FMS",
          desc: "$0.532",
          percentValue: "$397.4",
          percen: "5.82%",
          trade: "trade-up"),
    .init(id: 1,
          image: "polygon",
          name: "Polygon",
          value: "5387 Matic",
          desc: "$0.9221",
          percentValue: "$4,968.16",
          percen: "6.41%",
          trade: "trade-up"),
    .init(id: 2,
          image: "tether",
          name: "Tether",
          value: "256.7 USDT",
          desc: "$0.9997",
          percentValue: "$5,047.24",
          percen: "4.23%",
          trade: "trade-down"),
    .init(id: 3,
          image: "farm",
          name: "Farmsent",
          value: "747 FMS",
          desc: "$0.532",
          percentValue: "$397.4",
          percen: "5.82%",
          trade: "trade-up"),
    .init(id: 4,
          image: "polygon",
          name: "Polygon",
          value: "5387 Matic",
          desc: "$0.9221",
          percentValue: "$4,968.16",
          percen: "6.41%",
          trade: "trade-up"),
    .init(id: 5,
          image: "tether",
          name: "Tether",
          value: "256.7 USDT",
          desc: "$0.9997",
          percentValue: "$5,047.24",
          percen: "4.23%",
          trade: "trade-down")
]

