//
//  CollectionView.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

struct SellView: View {
    var body: some View {
        HStack {
            Image("sell")
                .resizable()
                .frame(width: 70, height: 70)
                .padding([.top, .leading, .trailing], 15)
                .padding(.bottom, 30)
            VStack(alignment: .leading) {
               
                    Text("Got Produce?")
                        .padding([.top], 10)
                   
               
                Button(action: {
                    print("Collection Cell")
                }, label: {
                    Text("Sell Them Here!")
                        .foregroundColor(.white)
                        .padding(.all, 5)
                })
                .background(Color.black)
                .cornerRadius(5)
                .padding([.leading], 0)
                .padding(.bottom, 20)
            }
            Spacer()
        }
    }
}

struct CoinView: View {
    var body: some View {
        HStack {
            Image("usd-coin")
                .resizable()
                .frame(width: 60, height: 60)
            VStack(alignment: .leading, spacing: 0) {
                Text("USD Coin")
                Text("Recived Just Now")
                    .padding([.top, .bottom], 10)
            }
            .padding([.leading], 10)
            Spacer()
            VStack(alignment: .trailing, spacing: 0) {
                Text("254 FMS")
                    .padding([.trailing], 10)
                Text("$42.55")
                    .padding([.trailing, .top, .bottom], 10)
            }
           
        }
        .padding([.top, .leading], 15)
        .padding(.bottom, 30)
    }
}
