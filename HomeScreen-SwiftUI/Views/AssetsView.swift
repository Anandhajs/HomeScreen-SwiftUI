//
//  AssetsView.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

struct AssetView: View {
    var image: String
    var name: String
    var value: String
    var desc: String
    var percentValue: String
    var percen: String
    var trade: String
    var body: some View {
        HStack {
            Image(image)
                .resizable()
                .frame(width: 50, height: 50, alignment: .center)
                .padding([.leading, .bottom, .top], 10)
            VStack {
                Text(name)
                    .multilineTextAlignment(.leading)
                    .padding([.leading, .bottom, .top], 10)
                Text(desc)
                    .multilineTextAlignment(.leading)
                    .padding([.bottom, .leading], 10)
            }
            VStack {
                HStack {
                    Spacer()
                    Text(value)
                        .multilineTextAlignment(.trailing)
                        .padding([.trailing, .top], 10)
                }
                
                HStack {
                    Spacer()
                    Image(trade)
                        .resizable()
                        .frame(width: 20, height: 20)
                        .padding([.bottom], 10)
                    
                    Text(percen)
                        .multilineTextAlignment(.trailing)
                        .padding([.bottom], 10)
                    
                    Text(percentValue)
                        .multilineTextAlignment(.trailing)
                        .padding([.trailing, .bottom, .leading], 10)
                }
            }
            Spacer()
        }
    }
}

//struct AssetView_Previews: PreviewProvider {  
//    static var previews: some View {
//       AssetView()
//    }
//}
