//
//  CurrencyView.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 03/02/23.
//

import SwiftUI

struct CurrencyView: View {
    var total: String = "$4546845"
    var screenSize: CGRect
    var body: some View {
        
        VStack(alignment: .leading, spacing: 0) {
                Text("Total Value")
                    .foregroundColor(Color(AppColor.themeColor().cgColor))
                    .padding([.top, .leading], 15)
                    .padding([.bottom], 10)
            
            ZStack {
                Button(action: {
                    print("drop")
                }, label: {
                    Text("")
                        .foregroundColor(.red)
                })
                HStack {
                    Text(total)
                        .foregroundColor(Color.white)
                        .padding()
                    Image("country")
                        .resizable()
                        .frame(width: 25, height: 20, alignment: .center)
                    Image("down-icon")
                        .resizable()
                        .frame(width: 20, height: 15, alignment: .center)
                        .foregroundColor(.white)
                        .padding([.leading], 5)
                        .padding([.trailing], 10)
                    Spacer()
                }
            }
            
            HStack {
                ZStack {
                    
                    Button(action: {
                        print("Sell")
                    }, label: {
                        Text("")
                        
                    })
                    .frame(width: (screenSize.width - 20) / 4, height: 80)
                    .padding(.all, 0)
                    VStack {
                        Image("send-icon")
                        Text("Send")
                            .foregroundColor(.white)
                    }
                    .padding([.bottom], 10)
                }
                ZStack {
                    Button(action: {
                        print("receive")
                    }, label: {
                        Text("")
                        
                    })
                    .frame(width: (screenSize.width - 20) / 4, height: 80)
                    .padding(.all, 0)
                    VStack {
                        Image("receive-icon")
                        Text("Receive")
                            .foregroundColor(.white)
                    }
                    .padding([.bottom], 10)
                }
                ZStack {
                    Button(action: {
                        print("Buy")
                    }, label: {
                        Text("")
                    })
                    .frame(width: (screenSize.width - 20) / 4, height: 80)
                    .padding(.all, 0)
                    VStack {
                        Image("buy-icon")
                        Text("Buy")
                            .foregroundColor(.white)
                    }
                    .padding([.bottom], 10)
                }
                ZStack {
                    Button(action: {
                        print("convert")
                    }, label: {
                        Text("")
                    })
                    .frame(width: (screenSize.width - 20) / 4, height: 80)
                    .padding(.all, 0)
                    VStack {
                        Image("convert-icon")
                        Text("Convert")
                            .foregroundColor(.white)
                    }
                    .padding([.bottom], 10)
                }
            }
            .padding([.top], 15.0)
        }
    }
}
