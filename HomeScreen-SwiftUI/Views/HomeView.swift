//
//  HomeView.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 03/02/23.
//

import SwiftUI

struct HomeView: View {
    var notifyImage: String
    var menuImage: String
    var name: String
    var desc: String
    var imageName: String
    
    var body: some View {
        HStack {
            Image(imageName)
                .resizable()
                .frame(width: 60, height: 60)
                .clipShape(Circle())
                .padding([.top], 10)
                .padding([.leading, .bottom], 15)
            
            VStack(spacing: 0) {
                HStack {
                    
                    Text(name)
                        .foregroundColor(.white)
                    Image("tag-icon")
                        .resizable()
                        .frame(width: 15, height: 15)
                }
                Text(desc)
                    .foregroundColor(.white)
                    .padding([.top, .bottom], 10)
                
            }
            .padding([.top, .leading], 10)
            Spacer()
            Button(action: {
                print("notify")
            }, label: {
                Image("notification")
                    .resizable()
                    .frame(width: 20, height: 20)
            })
            .padding()
            Button(action: {
                print("menu")
            }, label: {
                Image("menu")
                    .resizable()
                    .frame(width: 20, height: 20)
                    .padding([.trailing], 15)
                
            })
        }
    }
}
