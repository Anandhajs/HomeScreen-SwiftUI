//
//  CornerRadius.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

// MARK :- corner Radius for All corners
struct CornerRadius: Shape {
    var radius = CGFloat.infinity
    var corners = UIRectCorner.allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}
