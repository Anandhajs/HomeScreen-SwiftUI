//
//  AppColor.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

class AppColor {
    public static func themeColor() -> UIColor {
        return UIColor.init(hex: "#AC7046") ?? UIColor()
    }
    
    public static func backgroundColor() -> UIColor {
        return UIColor.init(hex: "#231F1B") ?? UIColor()
    }
    
    public static func cellColor() -> UIColor {
        return UIColor.init(hex: "#F7F0EB") ?? UIColor()
    }
    
    public static func pageTintColor() -> UIColor {
        return UIColor.init(hex: "#DABEAB") ?? UIColor()
    }
    
    public static func pageSelectColor() -> UIColor {
        return UIColor.init(hex: "#AC7046") ?? UIColor()
    }
}
