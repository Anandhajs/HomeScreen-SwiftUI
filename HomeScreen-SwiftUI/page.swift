//
//  page.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 04/02/23.
//

import SwiftUI

struct PageControl: UIViewRepresentable {
    
    var numberOfPages: Int
    @Binding var page: Int
    
func makeUIView(context: Context) -> UIPageControl {
    let view = UIPageControl()
    view.currentPageIndicatorTintColor = AppColor.pageSelectColor()
    view.pageIndicatorTintColor = AppColor.pageTintColor().withAlphaComponent(0.2)
    view.numberOfPages = numberOfPages
    return view
}
    func updateUIView(_ uiView: UIPageControl, context: Context) {
        DispatchQueue.main.async {
            uiView.currentPage = self.page
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        PageControl()
//    }
//}
