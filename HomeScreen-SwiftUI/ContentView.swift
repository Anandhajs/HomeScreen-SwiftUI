//
//  ContentView.swift
//  HomeScreen-SwiftUI
//
//  Created by Admin on 03/02/23.
//

import SwiftUI

struct ContentView: View {
    var screen = UIScreen.main.bounds
    @State var page: Int = 0
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            HomeView(notifyImage: "notification", menuImage: "menu", name: "Yog Shrusti", desc: "Farmer | Indonesia", imageName: "img")
                .background(Color(AppColor.backgroundColor().cgColor))
            ScrollView(showsIndicators: false) {
                CurrencyView(screenSize: screen)
                    .frame(width: screen.width)
                    .background(Color(AppColor.backgroundColor().cgColor))
                    .padding(.all, 0)
                    ScrollView(.horizontal, showsIndicators: false) {
                    
                    HStack(spacing: 10) {
                        ForEach(0..<2) { cell in
                          //  page = cell
                            if cell == 0 {
                                SellView()
                                    .frame(width: screen.width)
                            } else {
                                CoinView()
                                    .frame(width: screen.width)
                            }
                        }
                    }
                    .background(Color(AppColor.cellColor().cgColor))
                    .padding(.top, 0)

                }
                    PageControl(numberOfPages: 3, page: $page)
                        .padding(.top, -35)
                Spacer()
                HStack {
                    Text("Assets")
                        .padding(.leading, 15)
                        .padding(.top, 10)
                    Spacer()
                }
                LazyVStack {
                    ForEach(homeAssets, id: \.id) { asset in
                        AssetView(image: asset.image, name: asset.name, value: asset.value, desc: asset.desc, percentValue: asset.percentValue, percen: asset.percen, trade: asset.trade)
                            .background(CornerRadius(radius: 10, corners: .allCorners)
                                            .fill(Color(AppColor.cellColor().cgColor)))
                            .padding([.bottom], 10)
                        Spacer()
                    }
                }
                .padding([.leading, .trailing], 15)
            }
            .onAppear {
                UIScrollView.appearance().bounces = false
            }
        }
    }
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        ContentView()
//    }
//}
